Dit document is gedurende het project onderhevig aan veranderingen en toevoegingen, aan de hand van afspraken die binnen het team gemaakt worden of nieuwe specificaties vanuit het bedrijf. 

Tijden
Elke sprint komt het team bijeen om te reflecteren op het werkproces. Tijdens dit moment worden de sterke punten en verbeterpunten van het team verzameld. De planning voor de volgende sprint kan zo mogelijk plaatvinden op dezelfde dag, om meerdere vergaderdagen te voorkomen.

Er wordt gestreefd de gehele werkweek te werken aan een sprint, maar ook het weekend kan hierbij betrokken worden. Het uitgangspunt is het leveren van een increment die de vaardigheid en de capaciteit van het team tonen. In de regel dient ieder lid zijn taken af te ronden waar men overeen is gekomen bij de sprintplanning, maar elk lid dient zijn inzicht en zelfsturing te gebruiken om een passende bijdrage te leveren aan het project. Dit kan eventueel toegelicht worden bij de dagelijkse stand-ups. 

Persoonlijke situatie:
Tobias Waasdijk – Geen bijzonderheden
Yoell – Donderdag werkdag en avonduren. 

Streefpunten:
Stand-up niet meer dan 5 minuten te laat
Vergadering niet meer dan 15 minuten te laat


Stakeholders
-	Scrum team
-	Product Owner
-	Bedrijf
-	Docententeam AD software development

Scrum
Er wordt uitgegaan van sprinttrajecten van 2 weken. Dit biedt naar verwachting voldoende contactmomenten en flexibiliteit voor sturing van het project.

Er wordt gepoogd het bedrijf zoveel mogelijk fysiek te kunnen betrekken bij de sprint reviews. Indien dit niet mogelijk is voor een sprint, kan een demo doorgegeven worden voor het verkrijgen van feedback.

Stand-ups vinden dagelijks plaats in de ochtend via het meest gangbare kanaal. Dit kan fysiek zijn op lesdagen, een belgesprek op Discord, of – wanneer geen van beide hiervan mogelijk zijn – een bericht op Discord.

Communicatie
Interne communicatie zal voornamelijk plaatsvinden op Discord. Er is hiervoor gekozen vanwege de hoge beschikbaarheid en het gemak voor het vinden en delen van informatie.

Communicatie met het bedrijf zal voornamelijk plaatsvinden op WhatsApp en met Teams videobellen. Eventuele face-to-face ontmoetingen kunnen plaatsvinden in stad of Almere, afhankelijk van de beschikbaarheid van de partijen.

Ziekte of afwezigheid dient vooraf gemeld te worden op Discord of bij de stand-up. 

Documentatie
Interne (scrum) documentatie wordt in markdown gezet op Gitlab, zodat versiebeheer overzichtelijk blijft. Dit zorgt dat het projectverloop beter nagevolgd kan worden. De DevOps omgeving kan links bevatten om naar deze documentatie te verwijzen, zodat deze altijd actueel blijft.

Projectdocumenten die met het bedrijf gedeeld worden zullen in Word of andere Office software gemaakt of overgezet worden. Deze software biedt de meeste features, beste samenwerking en hoogste toegankelijkheid binnen het project.

Stand-ups worden zeer beknopt genotuleerd voor het navolgen van het projectverloop.

Rollen
Vanwege de kleinschaligheid van het team zullen de verantwoordelijkheden zoveel mogelijk gedeeld dienen te worden. Dit is essentieel voor de flexibiliteit en uptime van het team. Desondanks zullen de teamleden hun focus ergens anders leggen.

Tobias Waasdijk
-	Voornamelijke verantwoordelijke voor het op orde brengen van de documentatie.

Yoell 
-	Voornamelijke aanspreekpunt binnen de groep. 

Software
-	Azure DevOps
-	Microsoft Office
-	Gitlab
-	WhatsApp
-	Discord

Sancties
In verband met de kleinschaligheid van het team en het professionele verloop van het project, dienen conflicten zoveel mogelijk binnen het team opgelost te worden. Afspraken dienen duidelijk te vinden te zijn, en het navolgen van deze afspraken dient achterhaalbaar te zijn. Indien afspraken herhaaldelijk verbroken worden en hier geen oplossing voor gevonden kan worden binnen het team, kunnen er gevolgen zijn in overleg met docenten.
